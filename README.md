# Download Snapshot

`ppbf-synthetic-data` script download **guest**, **orders**, **session** and **event** snapshot entitiesto a local directory and apply transformations to generate PaddyPower synthetic data, for testing.

## How it works

For each entity type the script performs the steps:

  1. Creates **remote intermetiate temporal directory** structure in target host.
  2. **Download** snapshot files from `s3://boxever-data-${env}-${region}/snapshots/${date}/v2/${entity}/${client_key}/` folder to **intermetiate directory**.
  3. Remove to *_*max files filter*.
  4. **Download** to local **target directory**.
  5. Apply **transformations** for PaddyPower.
  6. **Upload** to S3 systethic data folder.

## Requirements

- [AWS CLI](https://aws.amazon.com/cli/), to work with S3.
- [JQ](https://stedolan.github.io/jq/), to transform the JSON files.
- [snzip](https://github.com/kubo/snzip), to decompress Snappy files.
- Be logged to Boxever. [OneLogin](https://www.onelogin.com/).

### Optional: Install with brew

```console
brew install awscli jq snzip
```

## How to execute

```console
./ppbf-synthetic-data [-c <client_key>] [-e <comma separated entities>] [-n <environment>] [-r <region>] [-s <Snapshot_date>] [-d <target_Date>] [-t <target_host>] [-l <local_dir>] [-x <maX_files>] [-h]
```

Parameters:

| Parameter            | Description                                | Default                                          |
|----------------------|--------------------------------------------|--------------------------------------------------|
| `-c <Client_key>`    | The client key to download the snapshot    | pqsPERS3lw12v5a9rrHPW1c4hET73GxQ                 |
| `-e <Entities>`      | Entities list. Comma separated values      | events,sessions,guests                           |
| `-n <eNvironment>`   | Environment                                | dev                                              |
| `-r <Region>`        | Region                                     | eu-west-1                                        |
| `-s <Snapshot_date>` | The snapshot date. Format: YYYY/MM/DD.     | yesterday                                        |
| `-d <target_Date>`   | The target date. Format: YYYY/MM/DD.       | today                                            |
| `-t <Target_host>`   | Intermediate target host                   | cruncher.v5.master.emr.eu-west-1.dev.boxever.com |
| `-t <Local_dir>`     | Local target directory                     | target                                           |
| `-x <maX_files>`     | Max files to process                       | 10                                               |
| `-h`                 | Shows help message                         |                                                  |
