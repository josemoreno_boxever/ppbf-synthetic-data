#!/bin/bash
#
# Perform Spinarcher -> PaddyPower transformations.

# Check that is load as library, not executed as shell script.
if [[ $(basename ${0#-}) = $(basename ${BASH_SOURCE}) ]]; then
  echo "${0} is a shell library, not a script!" >& 2
  exit 1
fi

# load libraries
. utils.sh

# Variables
paddypower_client_key="pPBFRdxAR61DXgGaYvvIPWQ7pAaq8QQJ"

function ppbf::random_guest_type() {
    local array=(customer visitor)
    echo "${array[$(utils::random ${#array[*]})]}"
}

function ppbf::random_session_type() {
    local array=(CC WEB SESSION)
    echo "${array[$(utils::random ${#array[*]})]}"
}

function ppbf::random_session_channel() {
    local array=(BRANCH CC KIOSK MOBILE_APP MOBILE_WEB OFFLINE OTHER WEB UKN)
    echo "${array[$(utils::random ${#array[*]})]}"
}

function ppbf::random_session_cart_type() {
    local array=(BROWSED CONVERTED ABANDONED UNK)
    echo "${array[$(utils::random ${#array[*]})]}"
}

function ppbf::random_event_type() {
    local array=(add add_consumers add_contacts force_close identity login order_update payment pnr_record purchase search subscription deposit betline)
    echo "${array[$(utils::random ${#array[*]})]}"
}

function ppbf::random_event_channel() {
    local array=(BRANCH CALL_CENTRE KIOSK MOBILE_APP MOBILE_WEB OFFLINE OTHER WEB UKN)
    echo "${array[$(utils::random ${#array[*]})]}"
}

function ppbf::random_event_status() {
    local array=(PROCESSED UNPROCESSED UKN)
    echo "${array[$(utils::random ${#array[*]})]}"
}

function ppbf::random_order_channel() {
    local array=(branch cc kiosk mobile_app mobile_web offline other web ukn)
    echo "${array[$(utils::random ${#array[*]})]}"
}

function ppbf::random_order_status() {
    local array=(placed)
    echo "${array[$(utils::random ${#array[*]})]}"
}

function ppbf::random_order_bet_type() {
    local array=(single double lucky15)
    echo "${array[$(utils::random ${#array[*]})]}"
}

function ppbf::random_order_items_channel() {
    local array=(branch cc kiosk mobile_app mobile_web offline other web ukn)
    echo "${array[$(utils::random ${#array[*]})]}"
}

function ppbf::random_order_items_order_item_type() {
    local array=(betline)
    echo "${array[$(utils::random ${#array[*]})]}"
}

function ppbf::transform_events_to_paddypower() {
  cat $1 | \
      jq -c "select(.type == \"VIEW\")" | \
      jq -c ".clientKey |= \"${paddypower_client_key}\"" | \
      jq -c ".pointOfSale |= \"paddypower.com\"" | \
      jq -c ".eventType |= \"$(ppbf::random_event_type)\"" | \
      jq -c ".status |= \"$(ppbf::random_event_status)\"" | \
      jq -c ".channel |= \"$(ppbf::random_event_channel)\"" | \
      jq -c ".utmSource |= \"UTM Source\"" | \
      jq -c ".utmMedium |= \"UTM Medium\"" | \
      jq -c ".utmCampaign |= \"UTM Campaign\"" | \
      jq -c ".utr |= \"the UTR\"" | \
      jq -c ".eventTypeId |= 11" | \
      jq -c ".competitionId |= 12" | \
      jq -c ".eventId |= 13" | \
      jq -c ".marketId |= \"the event id\"" | \
      jq -c ".meetingId |= \"the meeting id\"" | \
      jq -c ".raceId |= \"the race id\"" \
    > "$1.json"
  rm $1
  mv "$1.json" $1
}

function ppbf::transform_guests_to_paddypower() {
  cat $1 | \
      jq -c '.pointOfSale |= "paddypower.com"' | \
      jq -c ".clientKey |= \"${paddypower_client_key}\"" | \
      jq -c ".guestType |= \"$(ppbf::random_guest_type)\""  \
    > "$1.json"
  rm $1
  mv "$1.json" $1
}

function ppbf::transform_sessions_to_paddypower() {
  cat $1 | \
      jq -c ".pointOfSale |= \"paddypower.com\"" | \
      jq -c ".clientKey |= \"${paddypower_client_key}\"" | \
      jq -c ".sessionType |= \"$(ppbf::random_session_type)\"" | \
      jq -c ".channel |= \"$(ppbf::random_session_channel)\"" | \
      jq -c ".cartType |= \"$(ppbf::random_session_cart_type)\"" | \
      jq -c ".firstPageUri |= \"the first page uri\"" | \
      jq -c ".lastPageUri |= \"the last page uri\"" | \
      jq -c ".numAdds |= 1" | \
      jq -c ".numCheckouts |= 2" | \
      jq -c ".numSearches |= 3" | \
      jq -c ".numViews |= 4" | \
      jq -c ".numConfirms |= 5" | \
      jq -c ".numPayments |= 6" | \
      jq -c ".numSelects |= 7" | \
      jq -c ".pagesViewed |= \"pages viewed\"" | \
      jq -c ".ipAddress |= \"192.168.0.1\"" | \
      jq -c ".country |= \"the country\"" | \
      jq -c ".city |= \"city\"" | \
      jq -c ".revenue |= 10" | \
      jq -c ".userAgent |= \"the user agent\"" | \
      jq -c ".browser |= \"the browser\"" | \
      jq -c ".operatingSystem |= \"the operating system\"" | \
      jq -c ".device |= \"device\"" | \
      jq -c ".deviceCategory |= \"device category\"" | \
      jq -c ".trafficSource |= \"traffic source\"" | \
      jq -c ".referrer |= \"referrer\"" | \
      jq -c ".utmCampaign |= \"UTM Campaign\"" | \
      jq -c ".utmContent |= \"UTM Content\"" | \
      jq -c ".utmMedium |= \"UTM MEdium\"" | \
      jq -c ".utmSource |= \"UTM Source\"" | \
      jq -c ".utmTerm |= \"UTM Term\"" | \
      jq -c ".errorCode |= \"error code\"" | \
      jq -c ".isLoggedIn |= \"is logged in\"" | \
      jq -c ".initFlow |= \"init flow\"" | \
      jq -c ".language |= \"language\"" | \
      jq -c ".firstPageUri |= \"pageURL\"" \
    > "$1.json"
  rm $1
  mv "$1.json" $1
}

function ppbf::transform_orders_to_paddypower() {
  cat $1 | \
      jq -c ".clientKey |= \"${paddypower_client_key}\"" | \
      jq -c ".channel |= \"$( ppbf::random_order_channel )\"" | \
      jq -c ".status |= \"$( ppbf::random_order_status )\"" | \
      jq -c ".cardType |= \"the card type\"" | \
      jq -c ".currency |= \"the currency\"" | \
      jq -c ".orderValue |= 1" | \
      jq -c ".paymentType |= \"the currency\"" | \
      jq -c ".pos |= \"paddypower.com\"" | \
      jq -c ".pointOfSale |= \"paddypower.com\"" | \
      jq -c ".betPlatform |= \"the bet platform\"" | \
      jq -c ".stake |= 1.0" | \
      jq -c ".odds |= 2.0" | \
      jq -c ".side |= \"the side\"" | \
      jq -c ".betType |= \"$( ppbf::random_order_bet_type )\"" | \
      jq -c ".orderItems[].orderItemType |= \"$( ppbf::random_order_items_order_item_type )\"" | \
      jq -c ".orderItems[].guestRef |= \"  $(uuidgen)\"" | \
      jq -c ".orderItems[].orderRef |= \"$(uuidgen)\"" | \
      jq -c ".orderItems[].orderItemRef |= \"$(uuidgen)\"" | \
        # TODO Get the "ref" field
      jq -c ".orderItems[].currency |= \"the currency\"" | \
        # TODO Get the "currencyCode" field
      jq -c ".orderItems[].description |= \"the description\"" | \
      jq -c ".orderItems[].vendor |= \"the vendor\"" | \
      jq -c ".orderItems[].status |= \"the status\"" | \
      jq -c ".orderItems[].channel |= \"$( ppbf::random_order_items_channel )\"" | \
      jq -c ".orderItems[].settledDate |= 1574438637624" | \
      jq -c ".orderItems[].stake |= 3.0" | \
      jq -c ".orderItems[].stake |= 4.0" | \
      jq -c ".orderItems[].eventTypeID |= \"the event type id\"" | \
      jq -c ".orderItems[].eventTypeSport |= \"the eevent type sport\"" | \
      jq -c ".orderItems[].competitionId |= \"the competition id\"" | \
      jq -c ".orderItems[].competitionName |= \"the competition name\"" | \
      jq -c ".orderItems[].eventID |= \"the event id\"" | \
      jq -c ".orderItems[].eventName |= \"the event name\"" | \
      jq -c ".orderItems[].eventStartTime |= 1574438637624" | \
      jq -c ".orderItems[].eventEndTime |= 1574439637624" | \
      jq -c ".orderItems[].eventVenue |= \"the event venue\"" | \
      jq -c ".orderItems[].eventCountry |= \"the event country\"" | \
      jq -c ".orderItems[].selectionID |= \"the selection id\"" | \
      jq -c ".orderItems[].selectionRunnerId |= \"the selection runner id\"" | \
      jq -c ".orderItems[].selectionName |= \"the selection name\"" | \
      jq -c ".orderItems[].selectionHandicap |= \"the selection handicap\"" | \
      jq -c ".orderItems[].market |= \"the market\"" | \
      jq -c ".orderItems[].marketTypeId |= \"the market type id\"" | \
      jq -c ".orderItems[].marketTypeName |= \"the market type name\"" | \
      jq -c ".orderItems[].eachWay |= true" | \
      jq -c ".orderItems[].inPlay |= true" | \
      jq -c ".orderItems[].BSP |= true" | \
      jq -c ".orderItems[].cashout |= true" | \
      jq -c ".orderItems[].payout |= true" | \
      jq -c ".orderItems[].result |= true" \
    > "$1.json"
  rm $1
  mv "$1.json" $1  
}

##############################################################################
# Transform an entity snapshot file to PaddyPower
# Global:
#   None
# Arguments:
#   $1: Entity type. Can be: "events", "guest", "sessions" or "orders"
#   $2: Snapshot entity file to transform
# Returns:
#   None
##############################################################################
function transform() {
  case "$1" in
    events)   ppbf::transform_events_to_paddypower $2 ;;
    guests)   ppbf::transform_guests_to_paddypower $2 ;;
    sessions) ppbf::transform_sessions_to_paddypower $2 ;;
    orders)   ppbf::transform_orders_to_paddypower $2 ;;
  esac
}
