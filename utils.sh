#!/usr/bin/env bash
#
# General purpose utilities

# Check that is load as library, not executed as shell script.
if [[ $(basename ${0#-}) = $(basename ${BASH_SOURCE}) ]]; then
  echo "${0} is a shell library, not a script!" >& 2
  exit 1
fi

# This function returns a random value between 0 and $1 (not included). Default max value is 2.
# To get the returned value: $(random <max_value>)
#
# Example: Create a rand variable with a random value between 0 and 5
#   rand=$(random 5)
function utils::random() {
  local value=$RANDOM
  let "value %= ${1:-2}" # Default value is 2
  echo $value
}
